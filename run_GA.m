function run_GA(SAVE_ON)
    %% alghorithm setup
    INIT_IMAGE_SIZE = [60 45];
    directory_name = create_directory();
    %% read pictures
    ORIG_im = imread(fullfile('im', 'from.jpg'));
    MONA_im = imread(fullfile('im', 'to.jpg'));
    
    % handle unusable images
    rate = size(ORIG_im, 1)/size(ORIG_im, 2);
    if (rate > 1.4) || (rate < 1.3)
       error('ERROR: FROM image rate should be 4:3! Crop it before using the program.');
    end
    % resize
    MONA = imresize(MONA_im, INIT_IMAGE_SIZE);
    ORIG = imresize(ORIG_im, INIT_IMAGE_SIZE);

    %% set up GUI and population
    gen_index = 0;
    pop = population(MONA, gen_index, INIT_IMAGE_SIZE);
    my_gui = GA_gui(MONA,  ORIG, pop.EVOLUTION_ROUNDS, SAVE_ON);
    my_gui.population = pop;
    my_gui.dir = directory_name;
    
    %% set up history variables
    total_data.mean_fit = zeros(1,pop.EVOLUTION_ROUNDS);
    total_data.median_fit = zeros(1,pop.EVOLUTION_ROUNDS);
    total_data.best_fit = zeros(1,pop.EVOLUTION_ROUNDS);
    

    %% run main
    for evolution_index = 1 : (pop.EVOLUTION_ROUNDS)
       if evolution_index == 1
           get_random_population (pop, ORIG);
           % get_random_population_colors(pop);
       else
           pop = develop_new_population(pop);
       end
       
       % interpret results
       count_fitness_of_all_individuals(pop);
       total_data.mean_fit(evolution_index) = mean([pop.individuals.fitness]);
       total_data.median_fit(evolution_index) = median([pop.individuals.fitness]);
       total_data.best_fit(evolution_index) = pop.best_ind_fitness;
       
       if (SAVE_ON)
        save_population(pop, directory_name, evolution_index);
       end
       
       % react on buttons
       if my_gui.should_end
           break;
       end
       react_on_buttons(my_gui);
       
       % change GUI data if GUI not closed
       if ishandle(my_gui.fig)
           my_gui.population = pop;
           my_gui.set_new_data(evolution_index, pop.best_ind_fitness,...
               pop.best_ind, total_data);           
       else
          break 
       end       
    end
    
    
    %% final clean up, wait for GUI to be closed
    try
        waitfor(my_gui.fig)
    catch ME
        disp(ME.message)
    end
end