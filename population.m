classdef population < handle
    %POPULATION: class describing population in GA
    properties (Constant)
        POPULATION_SIZE = 10;
        EVOLUTION_ROUNDS = 5000;
        TOURNAMENT_SIZE = 7;
        
        % best (COPY_RATE*population_size) individuals are copied 
        COPY_RATE = 0.05;
        % every MUTATION_INDEX-th individual out of 
        % (1 - (copy_rate)*population_size) individuals is mutated
        MUTATION_INDEX = 2;
        % rest is crossed over
        
        % in range 0 - 2, sets how much the first population randomly
        % differs from the original picture
        CHANGE_RATE = 0.5;
        % sets percentage of mutated fields in the picture
        MUTATATION_DELTA = 0.2;
        % sets the minimal bound of rand in mutation
        MUTATE_RAND_BASE = 0.05;
        % sets the maximal bound of rand
        % should be in <0, (1 - MUTATE_RAND_BASE)>
        MUTATE_RAND_MAX = 0.3;
        
        % probabilities of selection 1st, 2nd and 3rd in tournament
        % must add to 1 to work correctly
        SELECT_1ST = 0.6;
        SELECT_2ND = 0.3;
        SELECT_3RD = 0.1;        
    end
    
    properties (Access = public)
        %
        goal_pic
        best_ind
        best_ind_fitness
        %
        individuals
        %
        generation_index
        init_image_size
    end
    methods (Access = public)
        function pop = population(MONA, gen_index, init_im_size)
            % constructor for POPULATION class
            pop.generation_index = gen_index;
            pop.goal_pic = MONA;
            pop.best_ind_fitness = 0;
            pop.init_image_size = init_im_size;
        end
        
        function get_random_population(pop, orig_pic)
            % adds random values to original pic to create first population
            for ind_index = 1 : pop.POPULATION_SIZE
                rand_dif = 255*((pop.CHANGE_RATE)*rand( pop.init_image_size(1),...
                    pop.init_image_size(2), 3) - (pop.CHANGE_RATE/2));
                pic = uint8(single(orig_pic) + rand_dif);    
                pic(pic == 255) = uint8(255 - 255*pop.CHANGE_RATE*rand());
                pic(pic == 0) = uint8(0 + 255*pop.CHANGE_RATE*rand());
                pop.individuals(ind_index).pic = pic;
                pop.individuals(ind_index).fitness = 0;
            end            
        end 
        
        function get_random_population_colors(pop)
            % creates randomly colored rectangles of picture's size
            for ind_index = 1 : pop.POPULATION_SIZE
                pic = ones(pop.init_image_size(1), pop.init_image_size(2), 3);
                pic(:,:,1) = randi(255);
                pic(:,:,2) = randi(255);
                pic(:,:,3) = randi(255);
                pop.individuals(ind_index).pic = uint8(pic);
                pop.individuals(ind_index).fitness = 0;
            end            
        end    
                
        function new_pop = develop_new_population(pop)  
            new_pop = population(pop.goal_pic, pop.generation_index + 1,...
                pop.init_image_size);
            copy_amount = floor(pop.COPY_RATE*pop.POPULATION_SIZE);
            if copy_amount < 1
                copy_amount = 1;
            end
            % copy best
            [~, ind] = sort([pop.individuals(:).fitness], 'descend');
            best_of = pop.individuals(ind);
            for copy_index = 1:copy_amount
                new_pop.individuals(copy_index).pic = best_of(copy_index).pic;
                new_pop.individuals(copy_index).fitness = 0;                   
            end
            % crossover and mutate to fill the rest of the places in pop
            for new_pop_index = copy_amount + 1 : pop.POPULATION_SIZE
                if mod(new_pop_index, pop.MUTATION_INDEX) == 0
                    % mutate individual
                    ind = new_pop.individuals(new_pop_index - 1).pic;
                    new_pic = mutate(pop, ind);                    
                else
                    % crossover individual
                    ind1 = tournament_selection(pop);
                    ind2 = tournament_selection(pop);
                    new_pic = crossover(pop, ind1, ind2);
                end
                new_pop.individuals(new_pop_index).pic = new_pic;
                new_pop.individuals(new_pop_index).fitness = 0;            
            end
        end    
        
        function count_fitness_of_all_individuals(pop)
            % count fitness by finding mean difference of MONA and new ind.
            for ind_index = 1 : pop.POPULATION_SIZE
               dif = (abs(pop.goal_pic - pop.individuals(ind_index).pic))/255;
               pop.individuals(ind_index).fitness = mean2(dif);
               if pop.individuals(ind_index).fitness > pop.best_ind_fitness
                    pop.best_ind_fitness = pop.individuals(ind_index).fitness;
                    pop.best_ind = pop.individuals(ind_index).pic;
               end
            end
        end  
        
        function save_population(pop, dir, gen_index)
            % save individuals of each generation to the results directory
            dir_base = fullfile(dir, strcat('pop', num2str(gen_index)));
            mkdir(dir_base);
            for index = 1:pop.POPULATION_SIZE
                pic_name = strcat(num2str(index),'.jpg');
                pic_name = fullfile(dir_base, pic_name);
                imwrite(pop.individuals(index).pic, pic_name);
            end
        end
        
        function save_final_results(pop, dir)
            % saves constants determining algorithm's behavior to .mat file
            % saves final picture in last round of GA
            f_name = fullfile(dir, 'constants.mat');
            save(f_name, 'pop');
            pic_name = fullfile(dir, 'final_picture.jpg');
            imwrite(pop.best_ind, pic_name);
        end
        
    end
        
    methods (Access = private)        
        function sel_ind = tournament_selection(pop)
            % randomly choose (TOURNAMENT_SIZE) competitors
            c_index = randi([1, pop.POPULATION_SIZE], 1, pop.TOURNAMENT_SIZE);
            competitors = pop.individuals(c_index);
            % weighted probabilities -> sort competitors from best to worst
            % selection of 1st 60%, 2nd 30%, 3rd 10%
            ran_sel = rand();
            [~, ind] = sort([competitors(:).fitness], 'descend');
            competitors = competitors(ind);
            if ran_sel < pop.SELECT_1ST
                winner = competitors(1);
            elseif ran_sel < (pop.SELECT_1ST + pop.SELECT_2ND)
                winner = competitors(2);
            else
                winner = competitors(3);
            end           
            sel_ind = winner.pic;
        end      
                    
        function new_pic = mutate(pop, pic1)
            % change (MUTATATION_DELTA) % of fields by random values in
            % range that is defined by evolution progress
            rand_bound = pop.MUTATE_RAND_BASE + ...
                (1 )*pop.MUTATE_RAND_MAX; %1 -pop.generation_index/pop.EVOLUTION_ROUNDS
            mask = rand(size(pic1, 1), size(pic1, 2), size(pic1, 3));
            mask(mask >= (1 - pop.MUTATATION_DELTA)) = 1;
            mask(mask ~= 1) = 0;
            vals_to_add = 255*rand_bound*(2*rand(size(pic1, 1),...
                size(pic1, 2), size(pic1, 3)) - 1);
            new_pic = uint8(double(pic1) + (mask.*vals_to_add));    
            new_pic(new_pic == 255) = uint8(255 - 255*rand_bound*rand());
            new_pic(new_pic == 0) = uint8(0 + 255*rand_bound*rand());
        end
        
        function new_pic = crossover(pop, pic1, pic2)
            % randomly combine two individuals to create a new one
            choice = randi([0 1]);
            if choice == 0
                % simple crossover
                mask = randi([0 1]);
                r1 = pic1.*(mask);
                r2 = pic2.*uint8(~mask);
                new_pic = uint8(r1 + r2);
            else
                % arithmetical crossover
                comb_rate = rand();
                new_pic = uint8(pic1.*(comb_rate) + pic2.*(1 - comb_rate));
            end
            new_pic(new_pic == 255) = uint8(255 - 255*pop.MUTATE_RAND_BASE*rand());
            new_pic(new_pic == 0) = uint8(0 + 255*pop.MUTATE_RAND_BASE*rand());    
        end
        
    end
end