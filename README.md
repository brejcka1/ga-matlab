# Genetic Algorithm on pictures in MATLAB



## The repository consists of:
* directory im (storage of Mona Lisa picture and original photo)
* directory results (can contain results of last algorithm runs)
* matlab files
    * `GA_gui.m` - GUI class
    * `population.m` - GA class
    * `run_GA.m` - main function to run the program
    * `react_on_buttons.m` - helper function for main function
    * `create_directory.m` - helper function, creates new directory for saving results


## Running the program:
* open and run function `run_GA.m` in MATLAB, the syntax is `run_GA(bool enable_save)`
* you can change the original picture by replacing the file `im/from.jpg`
    * the picture's resolution rate should be 4:3 (or very close to 4:3)
    * to change resolution of pictures in algorithm, change the constant `INIT_IMAGE_SIZE`
        * note that the resolution should stay in rate 4:3
        * the higher the resolution is, the slower the algorhitm is
* all the constants that determine algorithm's behavior are placed in properties of `class Population`
* in case you want to save the results of each generation, set the `SAVE_ON` parameter to true
    * results of each generation are saved by function `save_population(pop, dir, gen_index)`
* final results are saved by function `save_final_results(pop, dir)`
        * final picture is saved to results directory and file `final_picture.jpg`
        * population variables are saved on the end of the run as well


## How does it work?
Basically like human evolution. When the algorithm starts, the first population 
is created by randomly modifying the original picture (`im/from.jpg`). Population is
set of individuals where in this case each individual is represented by picture
(3D matrix in matlab). After the population is created, each individual is tested
(see criterial function for more details) and his fitness value is saved. Best
individual from all is showed in GUI.

Then new population is developed from the last population. Certain percentage 
(usually around 10-20 %) of the best individuals is copied, the rest of the 
population is filled by cross-over and mutation. Cross-over excepts two individuals 
and creates a new one by making linear combination of these two individuals.
Mutation excepts one individual and randomly changes part of it (depends on the
constants and progress of the algorithm). The higher generation we process, the lower 
maximal mutation is allowed. All of the individuals that are entering cross-over 
or mutation are chosen by tournament selection.

Tournament selection randomly chooses certain number (e.g. 7) of individuals and sorts
them by their fitness values. Then it chooses with weighted probabilities
(e.g. 0.6 - 0.3 - 0.1) the best individual, the second one or the third one.
Chosen individual then enters cross-over.

## Criterial function
In this case the criterial function is very simple. We just compare the individual's
picture with Mona Lisa by subtracting these two matrices and finding the mean value.
The mean value is than scaled into range 0 - 1 (divided by 255). This is called 
fitness value and it represents the similarity to Mona Lisa.

## Other usages of GA
Genetic programming in general can be used in many fields. E.g. we can learn robot
to walk correctly by using some simulator and genetic programming. We would set up
some parameters that would define the way it moves and would vary in each individual.
The criterial function could be e.g. the distance that robot reach in certain period 
of time.

In general, genetic programming can create very outstanding and unusual 
individuals that are not probable to be created by other forms of machine learning
(like neural networks, etc.). The downside is that it takes a very long time to run
GA with usable results.













