function [] = react_on_buttons(my_gui)
   % function in main program, connects GUI and algorithm
   % reaction on GUI buttons 
   % pauses algorithm each round to allow fluent change of GUI
   % pause should be removed if using non-graphicaly for faster performance
   if (my_gui.do_play)
       pause(0.2)
   elseif (my_gui.do_pause)
       my_gui.do_pause = false;
       waitfor(my_gui, 'do_play', true);
       if (my_gui.do_next)
           my_gui.do_pause = true;
           my_gui.do_play = false;
           my_gui.do_next = false;
       end
   elseif (my_gui.do_next)
       my_gui.do_pause = true;
       my_gui.do_play = false;
       my_gui.do_next = false;
       pause(0.2)
   end
end