function dir_name = create_directory()
    %CREATE_DIRECTORY: creates new subdirectory in results directory.
    name_index = 1;
    dir_base = fullfile('results', 'res');
    for i = 1:999
        if i < 10
            add_on = '00';
        elseif i < 100
            add_on = '0';
        else
            add_on = '';
        end
        dir_name = sprintf('%s%s%d', dir_base, add_on, name_index);
        if exist(dir_name, 'dir') ~= 7
            break;
        end        
        name_index = name_index + 1;
    end
    mkdir(dir_name);   
end
%% Create directory
% Function *create_directory* creates new subdirectory 
% in results directory.
%% Decription
% New subdirectory will contain all plots, data backups and
% results of current run of the program. This ensures
% that data gotten from older runs of the program won't be lost.
%% Head of function
% no input arguments
%% Return values
% returns name of created directory