classdef GA_gui < handle
    properties (Access = public)
        % control
        should_end
        do_next
        do_play
        do_pause
        % variables
        fig
        current_round
        sim_percents
        counted
        %
        population
        dir
    end
    
    properties (Access = private)
        % pictures
        MONA
        ORIG
        % variables        
        TOTAL_ROUNDS
        SAVE_ON
        % boxes
        middle_panel
        top_title_box
        picture_panel
        state_text
        bottom_panel
        info_box
        info_text
    end
    
    properties (Access = private)
        % background color
        pic_pan_color
        st_pan_color
        b_but_color
        fig_color
        % text color
        t_color
        s_color
        % font sizes
        small_font
        med_font
        large_font
        % graph color
        curve1_color
        curve2_color
        curve3_color
    end
    
    methods (Access = public)
        function my_gui = GA_gui(mona_pic, orig_pic, ev_rounds, SAVE_ON)
            close all;
            my_gui.SAVE_ON = SAVE_ON;
            % set up control variables
            my_gui.should_end = false;
            my_gui.do_next = false;
            my_gui.do_pause = false;
            my_gui.do_play = true;
            
            % read and prepare pictures
            my_gui.MONA = mona_pic;
            my_gui.ORIG = orig_pic;
            my_gui.counted = orig_pic * 1.2;

            % set up variables
            my_gui.TOTAL_ROUNDS = ev_rounds;
            my_gui.sim_percents = 1;
            my_gui.current_round = 0;
            my_gui.setup_colors_and_fonts
            
            % figure
            my_gui.fig = figure('Units', 'Normalized', 'Position', [0, 0, 1, 1]);
            set(my_gui.fig,'CloseRequestFcn', @(src, event) my_gui.safe_close());
            my_gui.fig.Name = 'Genetic Algorithm GUI';
            my_gui.fig.Color = my_gui.fig_color;
            my_gui.fig.MenuBar = 'none';
            
            % set up objects
            my_gui.setup_top_panel
            my_gui.setup_pictures
            my_gui.setup_bottom_panel
        end
             
        function change_top_title(my_gui)   
            top_title = sprintf("Evolution round %d/%d", my_gui.current_round, my_gui.TOTAL_ROUNDS);
            set(my_gui.top_title_box, 'String', top_title);   
        end
        
        function change_middle_picture(my_gui)
            i2 = subplot(1,4,[2,3], 'Parent', my_gui.picture_panel);
            imshow(my_gui.counted, 'Parent', i2);
            title("Current individual", 'Color', my_gui.s_color, 'FontSize', my_gui.med_font)            
        end
        
        function change_percent_text(my_gui)
            tit = sprintf("Current similarity: %.2f%%", my_gui.sim_percents);
            set(my_gui.state_text, 'String', tit)
        end
        
        function change_total_data(my_gui, total_data, cur_round)
            tit = sprintf("Best fitness of this round: %.5f\nMean fitness of this round: %.5f\nMedian fitness of this round: %.5f",...
                total_data.best_fit(cur_round), total_data.mean_fit(cur_round), total_data.median_fit(cur_round));
            set(my_gui.info_text, 'String', tit)  
            
            % plot
            subplot(3, 1, [2,3], 'Parent', my_gui.info_box);
            hold on;
            x = 1:cur_round;    
            plot(x, total_data.best_fit(1:cur_round),...
                'Color', my_gui.curve3_color,...
                'LineWidth', 2.0,...
                'DisplayName', 'best fitness'...
                );
            hold on;
            plot(x, total_data.median_fit(1:cur_round),...
                'Color', my_gui.curve2_color,...
                'LineWidth', 2.0,...
                'DisplayName', 'median fitness'...
                );
            hold on;
            plot(x, total_data.mean_fit(1:cur_round),...
                'Color', my_gui.curve1_color,...
                'LineWidth', 2.0,...
                'DisplayName', 'mean fitness'...
                );
            legend('Location', 'eastoutside', 'AutoUpdate', 'off');
        end
        
        function set_new_data(my_gui, ev_index, best_fitness, best_indiv, total_data)
           my_gui.current_round = ev_index;
           my_gui.sim_percents = best_fitness*100;
           my_gui.counted = best_indiv;
           % change GUI
           change_top_title(my_gui);
           change_middle_picture(my_gui);
           change_percent_text(my_gui);
           change_total_data(my_gui, total_data, ev_index);
        end
    end 
    
    methods (Access = private)
        function setup_colors_and_fonts(my_gui)
            color1 = [0.925,0.671,0.165; 1,0.784,0.357; 0.557,0.373,0];
            color2 = [0.282,0.173,0.635; 0.388,0.294,0.686; 0.122,0.039,0.384];
            color3 = [0.098,0.553,0.553; 0.212,0.6,0.6; 0,0.333,0.333];
            % background
            my_gui.pic_pan_color = color1(2,:);
            my_gui.st_pan_color = color3(1,:);
            my_gui.b_but_color = color3(1,:);
            my_gui.fig_color = color3(3,:);
            % graph_colors
            my_gui.curve1_color = color1(2, :);
            my_gui.curve2_color = color3(2, :);
            my_gui.curve3_color = color2(2, :);
            % text
            my_gui.t_color = color2(3,:);
            my_gui.s_color = color2(3,:);
            % font sizes
            my_gui.small_font = 14;
            my_gui.med_font = 20;
            my_gui.large_font = 30;            
        end
        
        function setup_top_panel(my_gui)
            top_panel = uipanel(...
                'BorderType', 'none',...
                'BackgroundColor', my_gui.pic_pan_color,...
                'Units', 'Normalized',...
                'Position', [0.01 0.8 0.98 0.19]);

            my_gui.top_title_box = uicontrol(...
                'Units', 'Normalized', ...
                'Style', 'Text', ...
                'Parent', top_panel,...
                'Position', [0 0.05 1 0.5], ...
                'FontSize', my_gui.large_font, ...
                'HorizontalAlignment', 'center');
           
            set(my_gui.top_title_box, 'FontWeight', 'bold')
            set(my_gui.top_title_box, 'BackgroundColor', my_gui.pic_pan_color)
            set(my_gui.top_title_box, 'ForegroundColor', my_gui.t_color)            
            top_title = sprintf("Evolution round %d/%d", my_gui.current_round, my_gui.TOTAL_ROUNDS);
            set(my_gui.top_title_box, 'String', top_title);            
        end
        
        function setup_pictures(my_gui)
            my_gui.middle_panel = uipanel('Tag', 'picture_panel',...
                'BorderType', 'none',...
                'BackgroundColor', my_gui.pic_pan_color,...
                'Units', 'Normalized',...
                'Position', [0.01 0.148 0.981 0.653]);
            my_gui.info_box = uipanel('Tag', 'info_panel',...
                'BorderType', 'none',...
                'BackgroundColor', my_gui.fig_color,...
                'Parent', my_gui.middle_panel,...
                'Units', 'Normalized',...
                'Position', [0.5 0 0.5 1]);
            my_gui.picture_panel = uipanel('Tag', 'picture_panel',...
                'BorderType', 'none',...
                'BackgroundColor', my_gui.pic_pan_color,...
                'Parent', my_gui.middle_panel,...
                'Units', 'Normalized',...
                'Position', [0 0 0.5 1]);
            setup_info_box(my_gui);

            % Mona Lisa
            i1 = subplot(1,4,1, 'Parent', my_gui.picture_panel);
            imshow(my_gui.MONA, 'Parent', i1);
            title("Mona Lisa", 'Color', my_gui.s_color, 'FontSize', my_gui.small_font)
            % Individual
            i2 = subplot(1,4,[2, 3], 'Parent', my_gui.picture_panel);
            imshow(my_gui.counted, 'Parent', i2);
            title("Current individual", 'Color', my_gui.s_color, 'FontSize', my_gui.med_font)
            % Original
            i3 = subplot(1,4,4, 'Parent', my_gui.picture_panel);
            imshow(my_gui.ORIG, 'Parent', i3);
            title("Original photo", 'Color', my_gui.s_color, 'FontSize', my_gui.small_font)
        end
        
        function setup_info_box(my_gui)
            info_pan = uipanel('Tag', 'st_pan',...
                'BackgroundColor', my_gui.st_pan_color,...
                'BorderType', 'none',...
                'Parent', my_gui.info_box,...
                'Units', 'Normalized',...
                'Position', [0.1 0.7 0.8 0.25]);

            my_gui.info_text = uicontrol(...
                'Units', 'Normalized', ...
                'Style', 'Text', ...
                'Position', [0 0.05 1 0.75], ...
                'Tag', 'figureColor', ...,
                'Parent', info_pan,...
                'FontSize', my_gui.med_font, ...
                'HorizontalAlignment', 'center');
            set(my_gui.info_text, 'BackgroundColor', my_gui.st_pan_color)
            set(my_gui.info_text, 'ForegroundColor', my_gui.t_color)
            tit = sprintf("Mean fitness of this round: %d\nMedian fitness of this round: %d", 0, 1);
            set(my_gui.info_text, 'String', tit)  
            
            % plot
            subplot(3, 1, [2,3], 'Parent', my_gui.info_box);
            axis([0 my_gui.TOTAL_ROUNDS 0 1], 'auto y');
        end
        
        function setup_bottom_panel(my_gui)
            my_gui.bottom_panel = uipanel('Tag', 'picture_panel',...
                'FontSize', 12,...
                'BackgroundColor', my_gui.pic_pan_color,...
                'BorderType', 'none',...    
                'Units', 'Normalized',...
                'Position', [0.01 0.01 0.98 0.14]);

            my_gui.setup_buttons()
            my_gui.setup_state_panel()  
        end
        
        function setup_buttons(my_gui)
            control_buttons = uipanel(...
                'Units', 'Normalized', ...
                'BackgroundColor', my_gui.pic_pan_color,...
                'Parent', my_gui.bottom_panel,...
                'BorderType', 'none',...
                'Position', [0.5 0 0.5 1]);

            play_btn = uicontrol('Style', 'pushbutton',...
                'Units', 'Normalized', ...
                'String', 'Play',...
                'FontSize', my_gui.med_font,...
                'Parent', control_buttons,...
                'Position', [0.23 0.2 0.15 0.6],...
                'ForegroundColor', my_gui.t_color,....
                'BackgroundColor', my_gui.b_but_color);

            pause_btn = uicontrol('Style', 'pushbutton',...
                'String', 'Pause',...
                'Units', 'Normalized', ...
                'FontSize', my_gui.med_font,...
                'Parent', control_buttons,...
                'Position', [0.4 0.2 0.15 0.6],...
                'ForegroundColor', my_gui.t_color,...
                'BackgroundColor', my_gui.b_but_color);

            next_btn = uicontrol('Style', 'pushbutton',...
                'String', 'Next',...
                'Units', 'Normalized', ...
                'FontSize', my_gui.med_font,...
                'Parent', control_buttons,...
                'Position', [0.57 0.2 0.15 0.6],...
                'ForegroundColor', my_gui.t_color,...
                'BackgroundColor', my_gui.b_but_color);
            
            set(play_btn, 'Callback', @(src, event)my_gui.react_on_play());
            set(pause_btn, 'Callback', @(src, event)my_gui.react_on_pause());
            set(next_btn, 'Callback', @(src, event)my_gui.react_on_next());
        end
        
        function setup_state_panel(my_gui)
            state_pan = uipanel('Tag', 'st_pan',...
                'BackgroundColor', my_gui.fig_color,...
                'BorderType', 'none',...
                'Parent', my_gui.bottom_panel,...
                'Units', 'Normalized',...
                'Position', [0 0 0.5 1]);

            state_disp = uipanel('Tag', 'st_disp',...
                'BackgroundColor', my_gui.st_pan_color,...
                'BorderType', 'none',...
                'Parent', state_pan,...
                'Units', 'Normalized',...
                'Position', [0 0 0.99 0.94]);

            my_gui.state_text = uicontrol(...
                'Units', 'Normalized', ...
                'Style', 'Text', ...
                'Parent', state_disp,...
                'Position', [0 0.05 1 0.5], ...
                'FontSize', my_gui.med_font, ...
                'HorizontalAlignment', 'center');
            set(my_gui.state_text, 'BackgroundColor', my_gui.st_pan_color)
            set(my_gui.state_text, 'ForegroundColor', my_gui.t_color)
            tit = sprintf("Current similarity: %d%%", my_gui.sim_percents);
            set(my_gui.state_text, 'String', tit)      
        end
        
        function react_on_play(my_gui)
            my_gui.do_play = true;
        end
        
        function react_on_pause(my_gui)
            my_gui.do_pause = true;
            my_gui.do_play = false;
        end
        
        function react_on_next(my_gui)
            my_gui.do_next = true;
            my_gui.do_play = true;
        end

        function safe_close(my_gui)
            delete(my_gui.fig);
            save_final_results(my_gui.population, my_gui.dir);
        end   
        
        
    end
    
end